import React from 'react';
import {HomePage, BookingPage, FindJobPage} from '@containers'
const routes = [
    {
        path: '/',
        exact: true,
        name: 'Trang chủ',
        main: () => <HomePage />,
        // scrollTo: 'trang-chu'
    },
    {
        path: '/gioi-thieu',
        exact: true,
        name: 'Giới thiệu',
        main: () => <HomePage />,
        // scrollTo: 'bang-xep-hang'
    },
    {
        path: '/huong-dan',
        exact: false,
        name: 'Hướng dẫn',
        main: () => <HomePage />,
        // childs: [
        //     {
        //         path: 'bai-thi-1',
        //         name: 'bài thi 1',
        //         main: ()=><HomePage/>
        //     },
        //     {
        //         path: 'bai-thi-2',
        //         name: 'bài thi 2',
        //         main: ()=><HomePage/>
        //     },
        //     {
        //         path: 'bai-thi-3',
        //         name: 'bài thi 3',
        //         main: ()=><HomePage/>
        //     }
        // ]
    },
    {
        path: '/ho-tro',
        exact: true,
        name: 'Hỗ trợ',
        main: () => <HomePage />,
        // scrollTo: 'thu-vien-tem-xe'
    },
    {
        path: '/tim-viec',
        exact: true,
        name: 'Sàn việc làm',
        main: () => <HomePage />,
        // main: () => <FindJobPage />,
        // scrollTo: 'thu-vien-tem-xe'
    },
    {
        path: '/booking',
        exact: true,
        name: 'Tìm người',
        main: () => <HomePage />,
        // main: () => <BookingPage />,
        // scrollTo: 'thu-vien-tem-xe'
    }
];
export default routes;