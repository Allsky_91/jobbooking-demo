import React, { Component, Fragment } from "react";

export default class CountDownReceiveOtpCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countDownOtp: false,
      countDownValue: 60, //
      clearInterval: 0,
    };
  }
  UNSAFE_componentWillMount(){
      if(this.props.runCountDown){
          this.setState({
            countDownOtp: true
          });
        this.countDown(this.state.countDownValue);
      }
  }
  UNSAFE_componentWillReceiveProps(nextProps){
    if(nextProps.runCountDown){
        this.setState({
          countDownOtp: true
        });
      this.countDown(this.state.countDownValue);
    }
}
  sendOTp = () => {
    this.setState({
      countDownOtp: true
    });
    this.countDown(this.state.countDownValue);
  };
  countDown = value => {
    this.setState({
      clearInterval: setInterval(() => {
        const { countDownValue } = this.state;
        if (countDownValue > 1) {
          this.setState({
            countDownValue: countDownValue - 1
          });
        } else {
          clearInterval(this.state.clearInterval);
          this.setState({
            countDownValue: value,
            countDownOtp: false
          });
        }
      }, 1000)
    });
  };
  componentWillUnmount() {
    clearInterval(this.state.clearInterval);
  }
  render() {
    const { countDownOtp, countDownValue } = this.state;
    return (
      <Fragment>
        {countDownOtp ? (
          <button className={this.props.className}>
            {parseInt(countDownValue / 60)
              .toString()
              .padStart(2, "0")}{" "}
            : {(countDownValue % 60).toString().padStart(2, "0")}
          </button>
        ) : (
          <button className={this.props.className} onClick={this.sendOTp}>
            {this.props.children ? this.props.children : "Gửi lại mã"}
          </button>
        )}
      </Fragment>
    );
  }
}
