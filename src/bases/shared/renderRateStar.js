import React from "react";
const RenderRateStar = ({star}) => {
    let html = [];
    for (let i = 1; i <= 5; i++) {
      html.push(
        <i
          key={i}
          className={
            star >= i
              ? "fas fa-star"
              : star > i - 1
              ? "fas fa-star-half-alt"
              : "far fa-star"
          }
        ></i>
      );
    }
    return (
      <span className="star__content" style={{
        whiteSpace: "nowrap"
      }}>
        {html}
      </span>
    );
  };

  export default RenderRateStar;