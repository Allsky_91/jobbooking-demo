  export  const onCheckboxItem = (arrCollect, id) => {
    var index = findIndex(arrCollect, id);
    if (index >= 0) {
      arrCollect.splice(index, 1);
    } else {
      arrCollect.push(id);
    }
    return arrCollect;
  };
  export  const onCheckBoxAll = (arrCollect, arrShow) => {
    var length = 0;
    for (let arrItem of arrShow) {
      if (findIndex(arrCollect, arrItem.id) >= 0) {
        length++;
      }
    }
    if (length < arrShow.length) {
      for (let arrItem of arrShow) {
        if (findIndex(arrCollect, arrItem.id) < 0) {
          arrCollect.push(arrItem.id);
        }
      }
    } else {
      for (let arrItem of arrShow) {
        if (findIndex(arrCollect, arrItem.id) >= 0) {
          arrCollect.splice(findIndex(arrCollect, arrItem.id), 1);
        }
      }
    }
    return arrCollect;
  };
  export const findIndex = (arr, id) => {
    var result = -1;
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] === id) {
        result = i;
        break;
      }
    }
    return result;
  };
