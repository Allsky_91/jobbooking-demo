import React, { Component } from "react";

class SectionBenefit extends Component {
  render() {
    return (
      <section className="section-benefit">
        <div className="bs-container">
          <div className="bs-row">
            <div className="bs-col">
              <div className="module module-benefit">
                <p className="title__float">
                  <span data-aos="fade-left">lợi ích</span>
                </p>
                <div className="module-header">
                  <h2 className="title" data-aos="fade-up">Lợi ích - Jobbooking</h2>
                </div>
                <div className="module-content">
                  <div className="img">
                    <div className="benefit-main">
                      <img src={ require("@static/images/benefit-img/phone.gif")} alt=""/>
                      <span className="part main__floor"><img src={ require("@static/images/benefit-img/main_benefit.gif")} alt=""/></span>
                      <div className="part1 part">
                        <img src={ require("@static/images/benefit-img/shadow.gif")} alt=""/>
                        <span className="layer1 layer">
                          <img src={ require("@static/images/benefit-img/shadow_bottom.gif")} alt=""/>
                        </span>
                        <span className="layer2 layer">
                          <img src={ require("@static/images/benefit-img/shadow_bottom.gif")} alt=""/>
                        </span>
                        <span className="layer3 layer">
                          <img src={ require("@static/images/benefit-img/shadow_bottom.gif")} alt=""/>
                        </span>
                        <span className="layer4 layer">
                          <img src={ require("@static/images/benefit-img/layer_float.gif")} alt=""/>
                        </span>
                        <span className="layer__logo layer"><img src={require("@static/images/benefit-img/benefit_logo.gif")} alt=""/></span>
                        <div className="layer layer-round">
                          <span className="round__main"></span>
                          <span className="layer round__small"></span>
                          <div className="point-content">
                            <span className="round__point1"><img src={require("@static/images/benefit-img/run1.gif")} alt=""/></span>
                            <span className="round__point2"><img src={require("@static/images/benefit-img/run1.gif")} alt=""/></span>
                            <span className="round__point3"><img src={require("@static/images/benefit-img/run2.gif")} alt=""/></span>
                          </div>
                        </div>
                      </div>
                      <div className="part2 part">
                        <img src={ require("@static/images/benefit-img/chat_layer1.gif")} alt=""/>
                        <span className="layer layer1">
                          <img src={ require("@static/images/benefit-img/chat_layer2.gif")} alt=""/>
                        </span>
                      </div>
                      <div className="part3 part">
                        <div className="part3-layer">
                          <img src={ require("@static/images/benefit-img/chat-left/part3.gif")} alt=""/>
                          <span className="chat__item1 chat__item">
                            <img src={ require("@static/images/benefit-img/chat-left/part2.gif")} alt=""/>
                          </span>
                          <span className="chat__item2 chat__item">
                          <img src={ require("@static/images/benefit-img/chat-left/part1.gif")} alt=""/>
                        </span>
                        </div>
                        <div className="layer part3-layer">
                          <img src={ require("@static/images/benefit-img/chat-left/part3.gif")} alt=""/>
                          <span className="chat__item1 chat__item">
                            <img src={ require("@static/images/benefit-img/chat-left/part2.gif")} alt=""/>
                          </span>
                          <span className="chat__item2 chat__item">
                          <img src={ require("@static/images/benefit-img/chat-left/part1.gif")} alt=""/>
                        </span>
                        </div>
                        <span className="layer layer2"><img src={ require("@static/images/benefit-img/chat-left/part4.gif")} alt=""/></span>
                      </div>
                      <div className="part4 part">
                        <img src={ require("@static/images/benefit-img/chat-right/part1.gif")} alt=""/>
                        <span className="layer layer1">
                          <img src={ require("@static/images/benefit-img/chat-right/part2.gif")} alt=""/>
                        </span>
                        <span className="layer layer2">
                          <img src={ require("@static/images/benefit-img/chat-right/part3.gif")} alt=""/>
                        </span>
                        <span className="layer layer3">
                          <img src={ require("@static/images/benefit-img/chat-right/part4.gif")} alt=""/>
                        </span>
                      </div>
                    </div>
                    <div className="layer-background">
                      <span className="layer__round3">
                        <img src={require("@static/images/benefit_layer3.gif")} alt="" data-aos="zoom-in" data-aos-delay="400"/>
                      </span>
                      <span className="layer__round2">
                        <img src={require("@static/images/benefit_layer2.gif")} alt="" data-aos="zoom-in" data-aos-delay="200"/>
                      </span>
                      <span className="layer1">
                        <img src={require("@static/images/benefit_layer1.gif")} alt="" data-aos="zoom-in"/>
                      </span>
                    </div>
                  </div>
                  <div className="text">
                    {benefitContent.map((item, index) => {
                      return (
                        <BenefitItem
                          key={index}
                          icon={item.icon}
                          title={item.title}
                          content={item.content}
                          src={item.line}
                          dataAos={item.dataAos}
                          dataAosDelay={item.dataAosDelay}
                        />
                      );
                    })}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
export default SectionBenefit;

const benefitContent = [
  {
    icon: <img src={require("@static/images/icon_smart_contract.gif")} alt="" className="icon" />,
    title: "Hợp đồng thông minh",
    content:
      "Các điều khoản ràng buộc sẽ được hợp đồng thông minh xử lý một cách tự động và khách quan giúp đảm bảo quyền lợi tuyệt đối cho các bên",
    line: require("@static/images/benefit_item1_line.gif"),
    dataAos: "fade-right",
    dataAosDelay: "1000"
  },
  {
    icon: <img src={require("@static/images/icon_saving.gif")} alt="" className="icon" />,
    title: "TIẾT KIỆM CHI PHÍ VÀ THỜI GIAN",
    content:
      "Bạn sẽ nhanh chóng đạt được kết quả của công việc do được hỗ trọ tối đa cách thức chọn người phù hợp",
    line:  require("@static/images/benefit_item2_line.gif"),
    dataAos: "fade-up",
    dataAosDelay: "1600"
  },
  {
    icon: <img src={require("@static/images/icon_income.gif")} alt="" className="icon" />,
    title: "TĂNG THU NHẬP",
    content:
      "Dễ dàng có được các công việc bán thời gian giúp tăng thu nhập trong các khoảng thời gian rảnh rỗi",
    line:  require("@static/images/benefit_item3_line.gif"),
    dataAos: "fade-up",
    dataAosDelay: "1400"
  },
  {
    icon: <img src={require("@static/images/benefit_icon1.gif")} alt="" className="icon" />,
    title: "Cắt giảm trung gian",
    content:
      "Dễ dàng có được các công việc bán thời gian giúp tăng thu nhập trong các khoảng thời gian rảnh rỗi",
    line:  require("@static/images/benefit_item4_line.gif"),
    dataAos: "fade-left",
    dataAosDelay: "1200"
  }
];

const BenefitItem = ({ icon, title, content, src, dataAos, dataAosDelay }) => {
  return (
    <div className="benefit-item" data-aos={dataAos} data-aos-delay={dataAosDelay}>
      <div className="content">
        <div className="before-layer">
          <div className="align-content">
            <span className="icon__layer">{icon}</span>
            <p className="title">{title}</p>
          </div>
        </div>
        <div className="after-layer">
          <div className="text">
            <p className="h4 title">{title}</p>
            <p className="desc">{content}</p>
          </div>
        </div>
      </div>
      <span className="benefit__line">
        <img src={src} alt="" />
      </span>
    </div>
  );
};
