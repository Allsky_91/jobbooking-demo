import React, { Component, Fragment } from "react";
import { BsTab } from "@basesShared";

var tab = [
  {
    label: "tab1",
    head: () => (
      <Fragment>
        <span className="img">
          <img
          src={require("@static/images/function_icon1.gif")}
            alt=""
            className="base__img"
          />
          <img
          src={require("@static/images/function_icon_hover1.gif")}
            alt=""
            className="hover__img"
          />
        </span>
        <p className="text">Đăng kí nhanh chóng</p>
      </Fragment>
    ),
    main: () => (
      <div className="tab-item1">
        <p className="desc">
        Chúng tôi hỗ trợ đăng ký bước đầu thông qua số điện thoại của bạn. Bạn chỉ cần xác thực mã OTP là có thể đăng ký nhanh chóng tài khoản trên Jobbooking để tiến hành trải nghiệm thực tế hệ thống của chúng tôi mà không hề gặp bất cứ phiền phức nào từ khâu đầu tiên.

        </p>
      </div>
    )
  },
  {
    label: "tab2",
    head: () => (
      <Fragment>
        <span className="img">
          <img
          src={require("@static/images/function_icon2.gif")}
            alt=""
            className="base__img"
          />
          <img
          src={require("@static/images/function_icon_hover2.gif")}
            alt=""
            className="hover__img"
          />
        </span>
        <p className="text">Thủ tục khoa học</p>
      </Fragment>
    ),
    main: () => (
      <div className="tab-item2">
        <p className="desc">
        Sau khi bạn đăng ký thành công, chúng tôi sẽ sử dụng tính năng xác minh danh tính thông qua tương tác online. Việc này giúp bạn có thể giups bạn ngồi tại nhà vẫn có thể thực hiện xác minh danh tính. Ở những yêu cầu cao hơn sau này khi bạn đã sử dụng hệ thống thành thạo và muốn nâng cao chỉ số xác minh chúng tôi sẽ tiến hành xác minh trực tiếp.
        </p>
      </div>
    )
  },
  {
    label: "tab3",
    head: () => (
      <Fragment>
        <span className="img">
          <img
          src={require("@static/images/function_icon3.gif")}
            alt=""
            className="base__img"
          />
          <img
          src={require("@static/images/function_icon_hover3.gif")}
            alt=""
            className="hover__img"
          />
        </span>
        <p className="text">Đăng việc dễ dàng</p>
      </Fragment>
    ),
    main: () => (
      <div className="tab-item3">
        <p className="desc">
        Sau khi đã xác minh danh tính, bạn đã có thể đăng công việc của bạn một cách nhanh chóng thông qua một giao diện đơn giản với các công cụ hỗ trợ hướng dẫn chuyên nghiệp và dễ hiểu,

        </p>
      </div>
    )
  },
  {
    label: "tab4",
    head: () => (
      <Fragment>
        <span className="img">
          <img
          src={require("@static/images/function_icon4.gif")}
            alt=""
            className="base__img"
          />
          <img
          src={require("@static/images/function_icon_hover4.gif")}
            alt=""
            className="hover__img"
          />
        </span>
        <p className="text">Kết nối thông minh</p>
      </Fragment>
    ),
    main: () => (
      <div className="tab-item4">
        <p className="desc">
        Người tìm việc sẽ dễ dàng và nhanh chóng tìm được các công việc phù hợp nhất với mình thông qua tính năng chọn lọc tự động của hệ thống căn cứ vào vị trí địa lý, thời gian, tính chất công việc, mức lương. Đồng thời cũng đồng nghĩa với việc người đăng việc sẽ tìm được đúng ứng viên mình mong muốn thông qua các căn cứ trên.

        </p>
      </div>
    )
  },
  {
    label: "tab5",
    head: () => (
      <Fragment>
        <span className="img">
          <img
          src={require("@static/images/function_icon5.gif")}
            alt=""
            className="base__img"
          />
          <img
          src={require("@static/images/function_icon_hover5.gif")}
            alt=""
            className="hover__img"
          />
        </span>
        <p className="text">Hiệu quả cao, rủi do hạn chế</p>
      </Fragment>
    ),
    main: () => (
      <div className="tab-item4">
        <p className="desc">
        Với lượng lượng người làm vô cùng phong phú và được xác minh cẩn trọng từ jobbooking chắc chắn sẽ khiến bạn hài lòng về hiệu quả của công việc. Việc mỗi bên đặt cọc một khoản tiền để đảm bảo và cam kết cũng giúp bạn hạn chế được các rủi do không đáng có
        </p>
      </div>
    )
  }
];
class SectionFunc extends Component {
  render() {
    return (
      <section className="section-function">
        <div className="bs-container">
          <div className="bs-row">
            <div className="bs-col">
              <div className="module module-function">
                <div className="module-content">
                  <BsTab {...tab} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
export default SectionFunc;
