import React, { Component } from "react";
import Slider from "react-animated-slider";
import "react-animated-slider/build/horizontal.css";

const content = [
  {
    title: "JOBBOOKING",
    title_part: "GIÁ TRỊ CỦA KẾT NỐI SỐ",
    desc:
      "Jobbooking sẽ kết nối những người đang có nhu cầu hoàn thành công việc với những người đang có nhu cầu làm thêm để gia tăng thu nhập bản thân.",
    button: "Tham gia ngay",
    linkTo: "/",
    image:  require("@static/images/banner_img.jpg"),
    align: "left"
  },
  {
    title: "JOBBOOKING",
    title_part: "GIÁ TRỊ CỦA KẾT NỐI SỐ",
    desc:
      "Jobbooking sẽ kết nối những người đang có nhu cầu hoàn thành công việc với những người đang có nhu cầu làm thêm để gia tăng thu nhập bản thân.",
    button: "Tham gia ngay",
    linkTo: "/",
    image:  require("@static/images/banner_img.jpg"),
    align: "center"
  },
  {
    title: "JOBBOOKING",
    title_part: "GIÁ TRỊ CỦA KẾT NỐI SỐ",
    desc:
      "Jobbooking sẽ kết nối những người đang có nhu cầu hoàn thành công việc với những người đang có nhu cầu làm thêm để gia tăng thu nhập bản thân.",
    button: "Tham gia ngay",
    linkTo: "/",
    image:  require("@static/images/banner_img.jpg"),
    align: "left"
  }
];
class SectionBanner extends Component {
  render() {
    return (
      <section className="section-banner">
      
        <Slider className="slider-wrapper">
          {content.map((item, index) => (
            <div
              key={index}
              className={`slider-content ${item.align === "center" ? "banner-center" : ""}`}
              style={{
                background: `url('${item.image}') no-repeat center center`
              }}
            >
              <div className="bs-container">
                <div className="module-banner">
                  <div className="module-content">
                      <div className="bs-row">
                        <div className={`bs-col  ${item.align === "center" ? "md-100" : "md-50"}`}>
                          <div className="banner-text">
                            <div className="inner">
                              <h1 className="title">
                                {item.title}
                                <span>{item.title_part}</span>
                              </h1>
                              <p className="desc">{item.desc}</p>
                              <button href="#" className="link" type="button">
                                {item.button} <span>&rarr;</span>
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                      </div>
               
                </div>
              </div>
            </div>
          ))}
        </Slider>
        <div className="banner-bottom">
          <svg className="wave-svg-box" viewBox="0 0 120 28">
                <defs>
                    <filter id="goo">
                        <feGaussianBlur in="SourceGraphic" stdDeviation="1" result="blur"></feGaussianBlur>
                        <feColorMatrix in="blur" mode="matrix" values="
                          1 0 0 0 0
                          0 1 0 0 0
                          0 0 1 0 0
                          0 0 0 13 -9" result="goo"></feColorMatrix>
                        <xfeblend in="SourceGraphic" in2="goo"></xfeblend>
                    </filter>
                    <path id="wave" d="M 0,10 C 30,10 30,15 60,15 90,15 90,10 120,10 150,10 150,15 180,15 210,15 210,10 240,10 v 28 h -240 z"></path>
                </defs>
                <path id="wave3" d="M 0,10 C 30,10 30,15 60,15 90,15 90,10 120,10 150,10 150,15 180,15 210,15 210,10 240,10 v 28 h -240 z" x="0" y="-2" className="wave" ></path>
                <path id="wave2" d="M 0,10 C 30,10 30,15 60,15 90,15 90,10 120,10 150,10 150,15 180,15 210,15 210,10 240,10 v 28 h -240 z" className="wave" x="0" y="0" ></path>
                <g className="gooeff" filter="url(#goo)">
                    <circle className="drop drop1" cx="10" cy="1" r="4.8"></circle>
                    <circle className="drop drop2" cx="15" cy="1.5" r="3.5"></circle>
                    <circle className="drop drop3" cx="16" cy="1.8" r="2.2"></circle>
                    <circle className="drop drop4" cx="18" cy="1" r="4.8"></circle>
                    <circle className="drop drop5" cx="12" cy="1.5" r="3.5"></circle>
                    <circle className="drop drop6" cx="16" cy="1.8" r="2.2"></circle>
                    <circle className="drop drop1" cx="4" cy="3.4" r="4.8"></circle>
                    <circle className="drop drop2" cx="4" cy="3.1" r="3.5"></circle>
                    <circle className="drop drop3" cx="7" cy="2.8" r="2.2"></circle>
                    <circle className="drop drop4" cx="2" cy="3.4" r="4.8"></circle>
                    <circle className="drop drop5" cx="6" cy="3.1" r="3.5"></circle>
                    <circle className="drop drop6" cx="9" cy="3.3" r="2.2"></circle>
                    <circle className="drop drop1" cx="0.2" cy="4.4" r="4.8"></circle>
                    <circle className="drop drop2" cx="4.2" cy="4.1" r="3.5"></circle>
                    <circle className="drop drop3" cx="2.2" cy="4.3" r="2.2"></circle>
                    <circle className="drop drop4" cx="2.2" cy="4.4" r="4.8"></circle>
                    <circle className="drop drop5" cx="13.2" cy="4.1" r="3.5"></circle>
                    <circle className="drop drop6" cx="16.2" cy="3.8" r="2.2"></circle>
                    <path id="wave1" d="M 0,10 C 30,10 30,15 60,15 90,15 90,10 120,10 150,10 150,15 180,15 210,15 210,10 240,10 v 28 h -240 z" className="wave" x="0" y="1" ></path>
                </g>
            </svg>
          </div>
      </section>
    );
  }
}
export default SectionBanner;
