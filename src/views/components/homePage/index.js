import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { SectionBanner, SectionFunc, SectionBenefit, SectionDownload, SectionNews } from "@containers";



class HomePage extends Component {
  // componentDidMount(){
  //   $("html, body").animate({
  //     scrollTop: 0
  //   }, 1000);
  // }
  render() {
    return (
      <main id="main">
        <Helmet>
          <title>Trang chủ</title>
        </Helmet>
        <SectionBanner />
        <SectionFunc/>
        <SectionBenefit/>
        <SectionDownload/>
        <SectionNews/>
      </main>
    );
  }
}

export default HomePage;


