import React, { Component } from "react";
import { Link } from "react-router-dom";
import { BsSlide } from "@basesShared";
class SectionNews extends Component {
  render() {
    return (
      <section className="section-news">
        <div className="bs-container">
          <div className="bs-row">
            <div className="bs-col">
              <div className="module module-news">
                <div className="module-header">
                  <h2 className="title" data-aos="fade-up">
                    Tin tức sự kiện
                  </h2>
                </div>
                <div
                  className="module-content"
                  data-aos="fade-up"
                  data-aos-delay="200"
                >
                  <p className="title__float">
                    <span data-aos="fade-left" data-aos-delay="200">
                      Tin tức
                    </span>
                  </p>
                  <div className="news-slide">
                    <BsSlide {...slideSettings}>
                      {newsDemo.map((item, index) => {
                        return (
                          <NewsItem
                            key={index}
                            title={item.title}
                            image={item.img}
                            desc={item.desc}
                            time={item.time}
                            id={item.id}
                          />
                        );
                      })}
                    </BsSlide>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
export default SectionNews;

const slideSettings = {
  slidesToShow: 3,
  slidesToScroll: 1,
  arrows: true,
  centerMode: false,
  centerPadding: "0",
  dots: false,
  fade: false,
  autoplay: false,
  infinite: false,
  draggable: false,
  swipeToSlide: false,
  touchMove: false,
  swipe: false,
  nextArrowSetting: {
    label: "&rarr;",
    className: "news__control prev__btn"
  },
  prevArrowSetting: {
    label: "&larr;",
    className: "news__control next__btn"
  },
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
};

const newsDemo = [
  {
    id: 1,
    img: <img src={require("@static/images/news_item1.jpg")} alt="" />,
    title: "Thực trạng xã hội về vấn đề việc làm",
    desc:
      "Không thể phủ nhận một điều rằng, vai trò cung cầu đối với thị trường lao động việc làm thêm đang ngày một gia tăng ở Việt Nam. Để tăng thêm thu nhập trang trải cho cuộc sống",
    time: "Dec 12, 2019"
  },
  {
    id: 2,
    img: <img src={require("@static/images/news_item2.jpg")} alt="" />,
    title: "Việc làm có vai trò quan trọng thế nào trong đời sống xã hội",
    desc:
      "Việc làm có vai trò quan trọng trong đời sống xã hội, nó không thể thiếu đối với từng cá nhân và toàn bộ nền kinh tế, là vấn đề cốt lõi và xuyên suốt trong các hoạt động kinh tế",
    time: "Dec 12, 2019",
    link: "#"
  },
  {
    id: 3,
    img: <img src={require("@static/images/news_item3.jpg")} alt="" />,
    title: "Áp dụng công nghệ thông tin vào tìm việc làm",
    desc:
      "Về vấn đề việc làm là vấn đề được tác động mạnh nhất bởi cuộc cách mạng công nghiệp 4.0. Điều đó đã thực sự làm phẳng thế giới, giúp nhiều người có thể dễ dàng tìm công việc từ xa",
    time: "Dec 12, 2019"
  },
  {
    id: 4,
    img: <img src={require("@static/images/news_item1.jpg")} alt="" />,
    title: "Thực trạng xã hội về vấn đề việc làm",
    desc:
      "Không thể phủ nhận một điều rằng, vai trò cung cầu đối với thị trường lao động việc làm thêm đang ngày một gia tăng ở Việt Nam. Để tăng thêm thu nhập trang trải cho cuộc sống",
    time: "Dec 12, 2019"
  },
  {
    id: 5,
    img: <img src={require("@static/images/news_item2.jpg")} alt="" />,
    title: "Việc làm có vai trò quan trọng thế nào trong đời sống xã hội",
    desc:
      "Việc làm có vai trò quan trọng trong đời sống xã hội, nó không thể thiếu đối với từng cá nhân và toàn bộ nền kinh tế, là vấn đề cốt lõi và xuyên suốt trong các hoạt động kinh tế",
    time: "Dec 12, 2019",
    link: "#"
  },
  {
    id: 6,
    img: <img src={require("@static/images/news_item3.jpg")} alt="" />,
    title: "Áp dụng công nghệ thông tin vào tìm việc làm",
    desc:
      "Về vấn đề việc làm là vấn đề được tác động mạnh nhất bởi cuộc cách mạng công nghiệp 4.0. Điều đó đã thực sự làm phẳng thế giới, giúp nhiều người có thể dễ dàng tìm công việc từ xa",
    time: "Dec 12, 2019"
  }
];
const NewsItem = ({ title, image, desc, time, id }) => {
  return (
    <div className="news-item">
      <div className="img">{image}</div>
      <div className="text">
        <p className="info">
          <img src={require("@static/images/icon_clock_small.gif")} alt="" />
          {time}
        </p>
        <p className="title">
          <Link to="/"> {title}</Link>
          {
            //<Link  to={`/news/${id}`}> {title}</Link>
          }
        </p>
        <p className="desc">{desc}</p>
      </div>
      <Link to="/" className="link"></Link>
      {
        // <Link to={`/news/${id}`} className="link"></Link>
      }
    </div>
  );
};
