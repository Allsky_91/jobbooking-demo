import React, { Component } from "react";
import { Link } from "react-router-dom";

class Footer extends Component {
  componentDidMount(){
  var canvas = document.getElementById('canvas');
  var footerTop = document.querySelector(".footer-top");
  canvas.width = footerTop.offsetWidth;
  canvas.height = footerTop.offsetHeight;
 var c = canvas.getContext("2d");


 var maxRadius = 20;
 var minRadius = 10;

function Circle(x,y,dx,dy, radius, color){
 this.x = x;
 this.y = y;
 this.dx = dx;
 this.dy = dy;
 this.radius = radius;
 this.draw = function () {
     c.beginPath();
     c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
     c.fillStyle = color;
     c.fill();
     c.closePath();
 }
 this.update = function () {
     if (this.x + this.radius > canvas.width || this.x - this.radius < 0) {
         this.dx = -this.dx;
     }
     if (this.y + this.radius > canvas.height || this.y - this.radius < 0) {
         this.dy = -this.dy;
     }
     this.x += this.dx;
     this.y += this.dy
     this.draw()
 }}
 var circleArray = []
 for (var i = 0; i <= 100; i++) {
     var color = "rgba(255,255,255,0.6)";
     var radius = Math.random()*5;
     var x = Math.random() * (canvas.width - radius * 2) + radius;
     var y = Math.random() * (canvas.height - radius * 2) + radius;
     var dx = (Math.random() - 0.5) * 2;
     var dy = (Math.random() - 0.5) * 2;
     circleArray.push(new Circle(x, y, dx, dy, radius, color));
 }

 function animate() {
     requestAnimationFrame(animate);
     c.clearRect(0, 0, canvas.width, canvas.height);
     canvas.width = footerTop.offsetWidth;
     canvas.height = footerTop.offsetHeight;
     circleArray[1].update();
     for (var i = 0; i < circleArray.length; i++) {
         circleArray[i].update();
     }
 }
 animate();
  }
  render() {
    return (
      <div id="footer">
     
      <div className="footer-style">
     
      <svg className="wave-svg-box" viewBox="0 0 120 28">
            <defs>
                <filter id="goo">
                    <feGaussianBlur in="SourceGraphic" stdDeviation="1" result="blur"></feGaussianBlur>
                    <feColorMatrix in="blur" mode="matrix" values="
                      1 0 0 0 0
                      0 1 0 0 0
                      0 0 1 0 0
                      0 0 0 13 -9" result="goo"></feColorMatrix>
                    <xfeblend in="SourceGraphic" in2="goo"></xfeblend>
                </filter>
                <path id="wave" d="M 0,10 C 30,10 30,15 60,15 90,15 90,10 120,10 150,10 150,15 180,15 210,15 210,10 240,10 v 28 h -240 z"></path>
            </defs>
            <path id="wave3" d="M 0,10 C 30,10 30,15 60,15 90,15 90,10 120,10 150,10 150,15 180,15 210,15 210,10 240,10 v 28 h -240 z" x="0" y="-2" className="wave" ></path>
            <path id="wave2" d="M 0,10 C 30,10 30,15 60,15 90,15 90,10 120,10 150,10 150,15 180,15 210,15 210,10 240,10 v 28 h -240 z" className="wave" x="0" y="0" ></path>
            <g className="gooeff" filter="url(#goo)">
                <circle className="drop drop1" cx="10" cy="1" r="4.8"></circle>
                <circle className="drop drop2" cx="15" cy="1.5" r="3.5"></circle>
                <circle className="drop drop3" cx="16" cy="1.8" r="2.2"></circle>
                <circle className="drop drop4" cx="18" cy="1" r="4.8"></circle>
                <circle className="drop drop5" cx="12" cy="1.5" r="3.5"></circle>
                <circle className="drop drop6" cx="16" cy="1.8" r="2.2"></circle>
                <circle className="drop drop1" cx="4" cy="3.4" r="4.8"></circle>
                <circle className="drop drop2" cx="4" cy="3.1" r="3.5"></circle>
                <circle className="drop drop3" cx="7" cy="2.8" r="2.2"></circle>
                <circle className="drop drop4" cx="2" cy="3.4" r="4.8"></circle>
                <circle className="drop drop5" cx="6" cy="3.1" r="3.5"></circle>
                <circle className="drop drop6" cx="9" cy="3.3" r="2.2"></circle>
                <circle className="drop drop1" cx="0.2" cy="4.4" r="4.8"></circle>
                <circle className="drop drop2" cx="4.2" cy="4.1" r="3.5"></circle>
                <circle className="drop drop3" cx="2.2" cy="4.3" r="2.2"></circle>
                <circle className="drop drop4" cx="2.2" cy="4.4" r="4.8"></circle>
                <circle className="drop drop5" cx="13.2" cy="4.1" r="3.5"></circle>
                <circle className="drop drop6" cx="16.2" cy="3.8" r="2.2"></circle>
                <path id="wave1" d="M 0,10 C 30,10 30,15 60,15 90,15 90,10 120,10 150,10 150,15 180,15 210,15 210,10 240,10 v 28 h -240 z" className="wave" x="0" y="1" ></path>
            </g>
        </svg>
      </div>
      <span className="footer__style"></span>
        <div className="footer-top">
        <canvas id="canvas"></canvas>
          <div className="bs-container">
            <div className="bs-row row-sm-15">
              <div className="bs-col md-25-15 xs-50-15">
                <div className="footer-logo" data-aos="zoom-out">
                  <Link to="/">
                    <img
                      src={ require("@static/images/logo_footer.gif")}
                      alt=""
                    />
                  </Link>
                  <p className="desc">
                    Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet
                  </p>
                  <div className="footer-social">
                    {footerSocial.map((item, index) => {
                      return (
                        <SocialItem
                          key={index}
                          link={item.link}
                          icon={item.icon}
                        />
                      );
                    })}
                  </div>
                </div>
              </div>
              <div className="bs-col md-25-15 xs-50-15">
                <div className="footer-link" data-aos="zoom-out" data-aos-delay="200">
                  <p className="title">Tin tức</p>
                  <ul className="footer-list">
                    {footerLink.map((item, index) => {
                      return (
                        <FooterListItem
                          key={index}
                          to={item.link}
                          label={item.label}
                        />
                      );
                    })}
                  </ul>
                </div>
              </div>
              <div className="bs-col md-25-15 xs-50-15">
                <div className="footer-link" data-aos="zoom-out" data-aos-delay="400">
                  <p className="title">Dịch vụ</p>
                  <ul className="footer-list">
                    {footerLink1.map((item, index) => {
                      return (
                        <FooterListItem
                          key={index}
                          to={item.link}
                          label={item.label}
                        />
                      );
                    })}
                  </ul>
                </div>
              </div>
              <div className="bs-col md-25-15 xs-50-15">
                <div className="footer-link" data-aos="zoom-out" data-aos-delay="600">
                  <p className="title">Liên hệ</p>
                  <ul className="footer-list">
                    {footerLink1.map((item, index) => {
                      return (
                        <FooterListItem
                          key={index}
                          to={item.link}
                          label={item.label}
                        />
                      );
                    })}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer-bottom">
          <div className="bs-container">
            <p className="desc" data-aos="fade-down">
              Bản quyền &copy; 2019 bởi Công ty CP <a href="https://bytesoft.vn/" className="link">Bytesoft</a> Việt Nam
            </p>
          </div>
        </div>
      </div>
    );
  }
}
export default Footer;
const footerLink = [
  {
    link: "#",
    label: "Lorem ipsum dolor"
  },
  {
    link: "#",
    label: "Lorem ipsum"
  },
  {
    link: "#",
    label: "Lorem ipsum dolor "
  },
  {
    link: "#",
    label: "Lorem ipsum "
  }
];
const footerLink1 = [
  {
    link: "#",
    label: "Lorem ipsum dolor"
  },
  {
    link: "#",
    label: "Lorem ipsum"
  },
  {
    link: "#",
    label: "Lorem ipsum dolor "
  },
  {
    link: "#",
    label: "Lorem ipsum "
  }
];
const footerSocial = [
  {
    to: "#",
    icon: "fab fa-facebook-f"
  },
  {
    to: "#",
    icon: "fab fa-twitter"
  },
  {
    to: "#",
    icon: "fab fa-vimeo-v"
  },
  {
    to: "#",
    icon: "fab fa-instagram"
  }
];
const SocialItem = ({ link, icon }) => {
  return (
    <div className="social-item">
      <a href={link} className="social__link">
        <i className={icon}></i>
      </a>
    </div>
  );
};
const FooterListItem = ({ to, label }) => {
  return (
    <li className="footer-list__item">
      <Link className="footer-list__link" to={to}>
        {label}
      </Link>
    </li>
  );
};
