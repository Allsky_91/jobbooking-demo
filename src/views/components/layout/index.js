import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import AOS from "aos";
import { Helmet } from "react-helmet";
import { Header, Footer, RegisterPage, OfficePage, NewsDetailsPage } from "@containers";
import routes from "@states/routes";

class Layout extends Component {
  componentDidMount() {
    crollShowHeader();
  }

  showRoutes = routes => {
    let result = null;
    if (routes.length > 0) {
      result = routes.map((route, index) => {
        return !route.childs ? (
          <Route
            path={route.path}
            component={route.main}
            key={index}
            exact={route.exact}
          />
        ) : (
          route.childs.map((routeChildren, index) => {
            return (
              <Route
                path={`${route.path}/${routeChildren.path}`}
                component={routeChildren.main}
                key={index}
                exact={routeChildren.exact}
              />
            );
          })
        );
      });
    }
    return result;
  };
  render() {
    return (
      <Router>
      <Helmet>
      <link rel="icon" type="image/png" href={require("@static/favicon.png")}></link>
    </Helmet>
        <Route  children={({history, match}) => <Header  match={match} history={history}/>}/>
        <Switch>
          {this.showRoutes(routes)}
        </Switch>
        <Footer />
      </Router>
    );
  }
}
export default Layout;

const checkHeader = (path, value) => {
  let result;
  if (path === value) {
    result = "home-header";
  } else {
    result = "";
  }
  return result;
};
const crollShowHeader = () => {
  AOS.init({
    duration: 500,
    offset: 0,
    delay: 200,
    easing: "ease-in-out",
    once: true,
    mirror: false
  });
  if ($(this).scrollTop() > 0) {
    $(".header-nav").addClass("scrolled");
  } else {
    $(".header-nav").removeClass("scrolled");
  }
  $(window).on("load", function() {
    if ($(this).scrollTop() > 0) {
      $(".header-nav").addClass("scrolled");
    } else {
      $(".header-nav").removeClass("scrolled");
    }
  });

  var lastScrollTop = 0;
  $(document).scroll(function() {
    if ($(this).scrollTop() > 0) {
      $("#header").addClass("scrolled");
      var st = $(this).scrollTop();
      if (st > lastScrollTop) {
        $("#header").addClass("scroll-bottom");
      } else {
        $("#header").removeClass("scroll-bottom");
      }
      lastScrollTop = st;
    } else {
      $("#header").removeClass("scrolled");
    }
  });
};
