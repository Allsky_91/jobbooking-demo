import React, { Component } from "react";
import { Route, Link } from "react-router-dom";
import routes from "@states/routes";
import { HeaderLogin } from "@containers";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggleMenu: false,
      login: false,
      account: {
        avatar: require("@static/images/news_item1.jpg"),
        userName: "Chung hán lương"
      }
    };
  }
  // componentDidMount() {
  //   $(document).scrollTop = 0;
  // }

  toggleMenu = () => {
    this.setState(
      {
        toggleMenu: !this.state.toggleMenu
      },
      () => {
        if (this.state.toggleMenu) {
          document.getElementById("root").classList.add("active-menu");
        } else {
          document.getElementById("root").classList.remove("active-menu");
        }
      }
    );
  };

  render() {
    const { toggleMenu, login, account } = this.state;
    const { location } = this.props.history;
    return (
      <div
        id="header"
        className={
          location.pathname !== "/login" && location.pathname !== "/register"
            ? location.pathname.indexOf("office") >= 0
              // ? "office-header"
              ? "office-header"
              : "color-header"
            : ""
        }
      >
        <div className="header-nav">
          <div className="bs-container">
            <div className="nav-content clearfix">
              <div className="nav-right" data-aos="fade-down">
                <HeaderLogin login={login} account={account} />
                <div className="language-item">
                  <div className="head">
                    <p className="title">EN</p>
                  </div>
                  <div className="content">
                    <ul className="language-list">
                      <li className="language-list__item">
                        <a href="#" className="language-item__link">
                          VI
                        </a>
                      </li>
                      <li className="language-list__item">
                        <a href="#" className="language-item__link">
                          EN
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="logo" data-aos="fade-down">
                <Link to="/">
                  <img src={require("@static/images/logo.gif")} alt="" />
                </Link>
              </div>

              {location.pathname.indexOf("office") >= 0 ? (
                <div className="office-right">
                  <div className="office-notify">
                    <div className="head" data-aos="fade-down">
                      <img
                        src={require("@static/images/icon_bell.gif")}
                        alt=""
                      />
                    </div>
                  </div>
                  {location.pathname === "/office/nguoi-tim-viec" ? (
                    <Link
                      className="office__link posting__btn"
                      to={{
                        pathname: "/"
                        // pathname: "/office/nguoi-dang-viec"
                      }}
                    >
                      Việc đã đăng
                    </Link>
                  ) : location.pathname === "/office/nguoi-dang-viec" ? (
                    <Link
                      className="office__link receiving__btn"
                      to={{
                        pathname: "/"
                        // pathname: "/office/nguoi-tim-viec"
                      }}
                    >
                      Việc đang nhận
                    </Link>
                  ) : ""}

                  <Link className="office__link booking__btn" 
                  // to="/booking"
                  to="/"
                  >
                    sàn tìm người
                  </Link>
                  <Link className="office__link job__btn"
                  //  to="/tim-viec"
                   to="/"
                   >
                    Sàn việc làm
                  </Link>
                </div>
              ) : (
                <div className="nav">
                  <span className="show__menu" onClick={this.toggleMenu}>
                    <i className="fas fa-bars" data-aos="fade-down" />
                  </span>
                  <div className={`menu ${toggleMenu ? "active" : ""}`}>
                    <span className="close__menu">
                      <i
                        className="far fa-window-close"
                        onClick={this.toggleMenu}
                      />
                    </span>
                    <ul className="menu-list clearfix">{showMenu(routes)}</ul>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Header;

const showMenu = menus => {
  var result = null;
  if (menus.length > 0) {
    result = menus.map((menu, index) => {
      return (
        <MenuLink
          key={index}
          label={menu.name}
          to={menu.path}
          activeOnlyWhenExact={menu.exact}
          scrollTo={menu.scrollTo}
          childs={menu.childs}
        />
      );
    });
  }
  return result;
};

const MenuLink = ({
  label,
  to,
  activeOnlyWhenExact,
  icon,
  scrollTo,
  childs
}) => {
  return (
    <Route
      path={to}
      exact={activeOnlyWhenExact}
      children={({ match, history }) => {
        var active = match ? "active" : "";
        if (icon) {
          return (
            <li className="menu-list__item" data-aos="fade-down">
              <Link to={to} className={`menu-list__link ${active}`}>
                <span className="nav__icon">
                  <i className={icon} />
                </span>
                {label}
              </Link>
            </li>
          );
        } else if (scrollTo) {
          var checkActive = history.location.search.slice(1);
          var active = checkActive === scrollTo ? "active" : "";
          return (
            <li className="menu-list__item" data-aos="fade-down">
              <Link
                to={{
                  pathname: to,
                  search: scrollTo
                }}
                className={`menu-list__link ${active}`}
                scroll-to={scrollTo}
              >
                {label}
              </Link>
            </li>
          );
        } else {
          var check = true;
          if (
            history.location.pathname === "/booking" ||
            history.location.pathname === "/tim-viec"
          ) {
            if (to == "/booking" || to == "/tim-viec") {
              check = false;
            }
          }
          if (check) {
            return (
              <li className="menu-list__item" data-aos="fade-down">
                <Link to={to} className={`menu-list__link ${active}`}>
                  {label}
                </Link>
                {childs ? (
                  <ul className="dropdown-list">
                    {childs.map((item, index) => {
                      return (
                        <Route
                          key={index}
                          path={to}
                          exact={activeOnlyWhenExact}
                          children={() => {
                            var activeDropdown =
                              `${to}/${item.path}` === location.pathname
                                ? "active"
                                : "";
                            return (
                              <li className="dropdown-list__item" key={index}>
                                <Link
                                  to={`${to}/${item.path}`}
                                  className={`dropdown-list__link ${activeDropdown}`}
                                >
                                  {item.name}
                                </Link>
                              </li>
                            );
                          }}
                        />
                      );
                    })}
                  </ul>
                ) : (
                  ""
                )}
              </li>
            );
          } else {
            return null;
          }
        }
      }}
    />
  );
};
