import React, { Component, Fragment } from "react";
import { NavLink, Link } from "react-router-dom";

class HeaderLogin extends Component {
  render() {
    const { login, account } = this.props;
    return (
      <Fragment>
        {login ? (
          <div className="user-item">
            <div className="img">
              <img src={account.avatar} alt="" />
              <span className="arrow">&rsaquo;</span>
            </div>
            <p className="user__name">{account.userName}</p>
            <div className="user-dropDown">
              <ul className="user-list">
                <li className="user-list__item">
                  <Link to="#" className="user-list__link">Đăng xuất</Link>
                </li>
                <li className="user-list__item">
                  <Link to="#" className="user-list__link">Đăng xuất</Link>
                </li>
                <li className="user-list__item">
                  <Link to="#" className="user-list__link">Đăng xuất</Link>
                </li>

              </ul>
            </div>
          </div>
        ) : (
          <div className="login-item">
            <NavLink
              // to="/register"
              to="/"
              className="login__link"
              activeClassName=""
              // activeClassName="active"
            >
              {" "}
              Đăng ký
            </NavLink>
            <NavLink
              // to="/login"
              to="/"
              className="login__link"
              activeClassName=""
              // activeClassName="active"
            >
              Đăng nhập
            </NavLink>
          </div>
        )}
      </Fragment>
    );
  }
}
export default HeaderLogin;
