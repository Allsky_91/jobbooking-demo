import React, { Fragment } from "react";
const Pagination = ({ list, paginationPage, itemPerPage, onClick }) => {
  const paginationLength = Math.floor(list.length / itemPerPage);
  const paginationRemainder = list.length % itemPerPage;
  return (
    <Fragment>
      {list.length > itemPerPage ? (
        <div className="list-pagination" data-aos="fade-up">
          <ul className="pagination">
            <li>
              <a
                className={paginationPage - 1 >= 0 ? "" : "disable"}
                onClick={() => {
                  paginationPage - 1 >= 0 ? onClick(paginationPage - 1) : null;
                }}
              >
                Prev
              </a>
            </li>
            {renderPagination(
              paginationLength,
              paginationPage,
              onClick,
              paginationRemainder
            )}
            <li>
              <a
                className={
                  paginationRemainder !== 0
                    ? paginationPage + 1 <= paginationLength
                      ? ""
                      : "disable"
                    : paginationPage + 1 < paginationLength
                    ? ""
                    : "disable"
                }
                onClick={() => {
                  paginationRemainder !== 0
                    ? paginationPage + 1 <= paginationLength
                      ? onClick(paginationPage + 1)
                      : null
                    : paginationPage + 1 < paginationLength
                    ? onClick(paginationPage + 1)
                    : null;
                }}
              >
                Next
              </a>
            </li>
          </ul>
        </div>
      ) : (
        ""
      )}
    </Fragment>
  );
};
export default Pagination;
const renderPagination = (
  paginationLength,
  paginationPage,
  onClick,
  paginationRemainder
) => {
  let html = [];
  if (paginationRemainder !== 0) {
    paginationLength = paginationLength + 1;
  }
  for (let i = 0; i < paginationLength; i++) {
    html.push(
      <li key={i} onClick={() => onClick(i)}>
        <a className={paginationPage === i ? "active" : ""}>{i + 1}</a>
      </li>
    );
  }
  return html;
};
