import React from "react";
import { Link } from "react-router-dom";

const SectionManage = () => {
  return (
    <section className="section-manage">
      <div className="bs-container">
        <div className="bs-row">
          <div className="bs-col">
            <div className="module module-manage">
              <div className="module-content">
                <div className="bs-row row-xs-15 row-tn-5">
                  {
                    manageList.map((item, index) => {
                      return <ManageItem key={index} link={item.link} icon={item.icon} title={item.title} dataAos={item.dataAos} dataAosDelay={item.dataAosDelay}/>
                    })
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
export default SectionManage;

const manageList = [
  {
    icon: "static/images/manage_icon1.gif",
    title: "Sàn tìm việc",
    link: "/tim-viec",
    dataAos: "fade-up",
    dataAosDelay: 200
  },
  {
    icon: "static/images/manage_icon2.gif",
    title: "Sàn tìm người",
    link: "booking",
    dataAos: "fade-up",
    dataAosDelay: 400
  },
  {
    icon: "static/images/manage_icon3.gif",
    title: "Việc đã đăng",
    link: "office/nguoi-dang-viec",
    dataAos: "fade-up",
    dataAosDelay: 600
  },
  {
    icon: "static/images/manage_icon4.gif",
    title: "Việc đang nhận",
    link: "office/nguoi-tim-viec",
    dataAos: "fade-up",
    dataAosDelay: 800
  }
]

const ManageItem = ({title, icon, link, dataAos, dataAosDelay}) => {
  return (
    <div className="bs-col sm-25-15 xs-50-15 tn-50-5" data-aos={dataAos} data-aos-delay={dataAosDelay}>
      <div className="manage-item" >
        <img src={icon} alt="" />
        <p className="title">{title}</p>
        <Link to={link} className="link"></Link>
      </div>
    </div>
  );
};
