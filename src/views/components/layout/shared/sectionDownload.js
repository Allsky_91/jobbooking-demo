import React from "react";

const SectionDownload = () => {
    return (
      <section className="section-download">
        <div className="bs-container">
          <div className="bs-row">
            <div className="bs-col">
              <div className="module module-download">
                <p className="title__float"><span data-aos="fade-left">Download app</span></p>
                <div className="module-header">
                  <h2 className="title" data-aos="fade-up">Download app</h2>
                  <p className="desc" data-aos="fade-up" data-aos-delay="200">
                    Đang phát triển...
                  </p>
                  <div className="download-button">
                    <a href="#" className="link" data-aos="fade-up" data-aos-delay="400" ><img src={require("@static/images/download_ios_btn.gif")} alt="" /></a>
                    <a href="#" className="link"  data-aos="fade-up" data-aos-delay="600" ><img src={require("@static/images/download_android_btn.gif")} alt=""/></a>
                  </div>
                </div>
                <div className="module-content">
                  <div className="download-img"  data-aos="fade-up" data-aos-delay="500">
                    <div className="img">
                      <img src={require("@static/images/download-img/main.gif")} alt=""/>
                      <div className="part1">
                        <img src={require("@static/images/download-img/layer1_base.gif")} alt=""/>
                        <span className="layer layer1"><img src={require("@static/images/download-img/layer1_behind1.gif")} alt=""/></span>
                        <span className="layer layer2"><img src={require("@static/images/download-img/layer1_behind2.gif")} alt=""/></span>
                        <span className="layer layer3"><img src={require("@static/images/download-img/layer1_phone.gif")} alt=""/></span>
                        <span className="layer layer4"><img src={require("@static/images/download-img/layer1_personal.gif")} alt=""/></span>
                        <div className="layer layer5">
                          <img src={require("@static/images/download-img/layer1_chat.gif")} alt=""/>
                          <span className="layer chat__point1"><img src={require("@static/images/download-img/layer1_chat_point.gif")} alt=""/></span>
                          <span className="layer chat__point2"><img src={require("@static/images/download-img/layer1_chat_point.gif")} alt=""/></span>
                          <span className="layer chat__point3"><img src={require("@static/images/download-img/layer1_chat_point.gif")} alt=""/></span>
                        </div>
                        <span className="layer layer6"><img src={require("@static/images/download-img/download_logo.gif")} alt=""/></span>
                      </div>
                      <div className="part2">
                        <div className="chat-send">
                          <img src={require("@static/images/download-img/chat-img/chat_main.gif")} alt=""/>
                          <span className="symbol symbol1"><img src={require("@static/images/download-img/chat-img/chat_symbol1.gif")} alt=""/></span>
                          <span className="symbol symbol2"><img src={require("@static/images/download-img/chat-img/chat_symbol2.gif")} alt=""/></span>
                          <span className="symbol symbol3"><img src={require("@static/images/download-img/chat-img/chat_symbol3.gif")} alt=""/></span>
                          <span className="symbol symbol4"><img src={require("@static/images/download-img/chat-img/chat_symbol4.gif")} alt=""/></span>
                          <span className="symbol symbol5"><img src={require("@static/images/download-img/chat-img/chat_symbol5.gif")} alt=""/></span>
                          <span className="symbol symbol6"><img src={require("@static/images/download-img/chat-img/chat_symbol6.gif")} alt=""/></span>
                        </div>
                        <div className="chat-screen">
                          <span className="layer layer1"><img src={require("@static/images/download-img/chat-img/chat_item1.gif")} alt=""/></span>
                          <span className="layer layer2"><img src={require("@static/images/download-img/chat-img/chat_item2.gif")} alt=""/></span>
                          <span className="layer layer3"><img src={require("@static/images/download-img/chat-img/chat_item3.gif")} alt=""/></span>
                          <span className="layer layer4"><img src={require("@static/images/download-img/chat-img/chat_item4.gif")} alt=""/></span>
                        </div>
                        <div className="chat-box">
                          <img src={require("@static/images/download-img/chat-img/chat1.gif")} alt=""/>
                          <div className="layer layer2"><img src={require("@static/images/download-img/chat-img/chat3.gif")} alt=""/></div>
                          <div className="layer layer1"><img src={require("@static/images/download-img/chat-img/chat2.gif")} alt=""/></div>
                        </div>
                      </div>
                      <span className="layer layer__part1"><img src={require("@static/images/download-img/layer1_part.gif")} alt=""/></span>
                      <span className="layer layer__part2"><img src={require("@static/images/download-img/layer1_part.gif")} alt=""/></span>
                      <span className="box__shadow1"><img src={require("@static/images/download-img/boxshadow_small.gif")} alt=""/></span>
                      <span className="box__shadow2"><img src={require("@static/images/download-img/boxshadow_big.gif")} alt=""/></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
}
export default SectionDownload;
