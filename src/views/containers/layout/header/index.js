import {Header} from '@components';
import { connect } from 'react-redux';

function mapStateToProps(state,props) {
    return {
    };
}

function mapDispatchToProps(dispatch,props) {
    return {
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);