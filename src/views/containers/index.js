// Layout
import Header from "./layout/header";
import HeaderLogin from "./layout/header/headerLogin";
import Footer from "./layout/footer";
import Layout from "./layout";
// shared component
import SectionDownload from './layout/shared/sectionDownload';
import SectionManage from './layout/shared/sectionManage';
import Pagination from './layout/shared/pagination';

// HomePage
import HomePage from './homePage';
import SectionFunc from './homePage/sectionFunc';
import SectionBenefit from './homePage/sectionBenefit';
import SectionNews from './homePage/sectionNews';
import SectionBanner from './homePage/sectionBanner';

export {
    Layout, Header, HeaderLogin, Footer, // Layout
    SectionDownload, SectionManage, Pagination,  // shared component
    HomePage, SectionBanner, SectionFunc, SectionBenefit,  SectionNews, // HomePage
}
