export const  optionsMoneyType = [
    {
      label: "VND",
      value: 0
    },
    {
      label: "USD",
      value: 1
    }
  ];
  
 export const optionsPayType = [
    {
      label: "Nhận công theo giờ",
      value: 0
    },
    {
      label: "Nhận công theo gói",
      value: 1
    }
  ];